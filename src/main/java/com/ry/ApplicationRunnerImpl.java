package com.ry;

import com.ry.common.utils.CaffeineCacheUtil;
import com.ry.common.utils.RedisCacheUtil;
import com.ry.kafka.KafkaConsumerThread;
import com.ry.rebuild.RebuildProductInfoCacheThread;
import com.ry.rebuild.RebuildShopInfoCacheThread;
import com.ry.spring.SpringContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationRunnerImpl implements ApplicationRunner, ApplicationContextAware {

    @Autowired
    private RedisCacheUtil redisCacheUtil;

    @Autowired
    private CaffeineCacheUtil caffeineCacheUtil;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("ApplicationRunnerImpl启动了");

        // 启动kafka消息消费线程
        new Thread(new KafkaConsumerThread("cache-message2")).start();

        // 启动商品基础信息缓存重建线程
        new Thread(new RebuildProductInfoCacheThread(redisCacheUtil,caffeineCacheUtil)).start();

        // 启动店铺基础信息缓存重建线程
        new Thread(new RebuildShopInfoCacheThread(redisCacheUtil,caffeineCacheUtil)).start();

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        // 赋值 ApplicationContext
        SpringContext.setApplicationContext(applicationContext);
    }
}
