package com.ry.common.enums;

import lombok.Getter;


@Getter
public enum CacheTypeEnum {

    PRODUCT_INFO(5),
    SHOP_INFO(20);

    private int expires;

    CacheTypeEnum(int expires) {
        this.expires = expires;
    }
}
