package com.ry.common.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ry.common.client.model.ShopInfo;


public interface ShopInfoMapper extends BaseMapper<ShopInfo> {

}
