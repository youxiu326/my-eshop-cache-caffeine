package com.ry.common.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ry.common.client.model.ProductInfo;

public interface ProductInfoMapper  extends BaseMapper<ProductInfo> {

}
