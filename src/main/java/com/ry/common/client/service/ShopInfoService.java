package com.ry.common.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ry.common.client.model.ShopInfo;


public interface ShopInfoService extends IService<ShopInfo> {

}
