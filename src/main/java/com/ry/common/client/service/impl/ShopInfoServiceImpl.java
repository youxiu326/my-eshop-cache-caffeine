package com.ry.common.client.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ry.common.client.mapper.ShopInfoMapper;
import com.ry.common.client.model.ShopInfo;
import com.ry.common.client.service.ShopInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ShopInfoServiceImpl extends ServiceImpl<ShopInfoMapper, ShopInfo> implements ShopInfoService {


}
