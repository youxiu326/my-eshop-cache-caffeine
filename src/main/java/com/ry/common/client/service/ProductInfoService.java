package com.ry.common.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ry.common.client.model.ProductInfo;


public interface ProductInfoService extends IService<ProductInfo> {

}
