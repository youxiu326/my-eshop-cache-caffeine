package com.ry.common.client;

import com.ry.common.client.model.ProductInfo;
import com.ry.common.client.model.ShopInfo;
import com.ry.common.client.service.ProductInfoService;
import com.ry.common.client.service.ShopInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 模拟远程商品基础服务feign
 *  和
 * 模拟远程店铺基础服务feign
 */
@Slf4j
@Component
public class BaseProductClient {

    @Autowired
    private ProductInfoService productInfoService;

    @Autowired
    private ShopInfoService shopInfoService;


    public ProductInfo queryProductInfoById(Long id){
        return productInfoService.getById(id);
    }

    public ShopInfo queryShopInfoById(Long id){
        return shopInfoService.getById(id);
    }

}
