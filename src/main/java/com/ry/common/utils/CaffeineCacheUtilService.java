package com.ry.common.utils;

import com.ry.common.client.model.ProductInfo;
import com.ry.common.client.model.ShopInfo;

/**
 * spring cache需要一个接口
 */
public interface CaffeineCacheUtilService {

    /**
     * 从Caffeine本地缓存获取店铺信息
     * @param id
     */
    public ShopInfo getShopInfoCache(Long id);

    /**
     * 从Caffeine本地缓存获取商品信息
     * @param id
     */
    public ProductInfo getProductInfoCache(Long id);

    /**
     * 将店铺信息保存到Caffeine本地缓存
     * @param shopInfo
     */
    public ShopInfo putShopInfoCache(ShopInfo shopInfo);

    /**
     * 将商品信息保存到Caffeine本地缓存
     * @param productInfo
     */
    public ProductInfo putProductInfoCache(ProductInfo productInfo);

}
