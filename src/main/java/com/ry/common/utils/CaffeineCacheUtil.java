package com.ry.common.utils;

import cn.hutool.json.JSONUtil;
import com.ry.common.client.model.ProductInfo;
import com.ry.common.client.model.ShopInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * Caffeine本地缓存工具类
 */
@Slf4j
@Component
public class CaffeineCacheUtil implements CaffeineCacheUtilService{

    /**
     * 从Caffeine本地缓存获取店铺信息
     * @param id
     */
    @Override
    @Cacheable(value = "SHOP_INFO", key = "'shop_info_'+#id")
    public ShopInfo getShopInfoCache(Long id){
        return null;
    }

    /**
     * 从Caffeine本地缓存获取商品信息
     * @param id
     */
    @Override
    @Cacheable(value = "PRODUCT_INFO", key = "'product_info_'+#id")
    public ProductInfo getProductInfoCache(Long id){
        return null;
    }

    /**
     * 将店铺信息保存到Caffeine本地缓存
     * @param shopInfo
     */
    @Override
    @CachePut(value = "SHOP_INFO", key = "'shop_info_'+#shopInfo.getId()")
    public ShopInfo putShopInfoCache(ShopInfo shopInfo){
        log.info("===将店铺信息保存至Caffeine本地缓存-> {}", JSONUtil.toJsonStr(shopInfo));
        return shopInfo;
    }

    /**
     * 将商品信息保存到Caffeine本地缓存
     * @param productInfo
     */
    @Override
    @CachePut(value = "PRODUCT_INFO", key = "'product_info_'+#productInfo.getId()")
    public ProductInfo putProductInfoCache(ProductInfo productInfo){
        log.info("===将商品信息保存至Caffeine本地缓存-> {}", JSONUtil.toJsonStr(productInfo));
        return productInfo;
    }

}
