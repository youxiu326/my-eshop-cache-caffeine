package com.ry.common.utils;

import cn.hutool.json.JSONUtil;
import com.ry.common.client.model.ProductInfo;
import com.ry.common.client.model.ShopInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Redis缓存工具类
 */
@Slf4j
@Component
public class RedisCacheUtil {

    private static final String PRODUCT_KEY = "ry:productInfo:";
    private static final String SHOP_KEY = "ry:shopInfo:";

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    /**
     * 避免覆盖其他项目的序列化，只在当前类实例化一个RedisTemplate
     */
    @PostConstruct
    public void init() {
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringRedisSerializer);
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
    }

    /**
     * 从redis获取店铺信息
     * @param id
     */
    public ShopInfo getShopInfoCache(Long id){
        String key = SHOP_KEY + id;
        ValueOperations<String,ShopInfo> vos = redisTemplate.opsForValue();
        ShopInfo shopInfo = vos.get(key);
        log.info("===从redis缓存中获取店铺信息-> {}", JSONUtil.toJsonStr(shopInfo));
        return shopInfo;
    }

    /**
     * 从redis获取商品信息
     * @param id
     */
    public ProductInfo getProductInfoCache(Long id){
        String key = PRODUCT_KEY + id;
        ValueOperations<String, ProductInfo> vos = redisTemplate.opsForValue();
        ProductInfo productInfo = vos.get(key);
        log.info("===从redis缓存中获取商品信息-> {}", JSONUtil.toJsonStr(productInfo));
        return productInfo;
    }

    /**
     * 将店铺信息保存到redis中
     * @param shopInfo
     */
    public void putShopInfoCache(ShopInfo shopInfo){
        String key = SHOP_KEY + shopInfo.getId();
        ValueOperations<String, ShopInfo> vos = redisTemplate.opsForValue();
        vos.set(key,shopInfo);
        log.info("===将店铺信息保存至redis缓存-> {}", JSONUtil.toJsonStr(shopInfo));
    }

    /**
     * 将商品信息保存到redis中
     * @param productInfo
     */
    public void putProductInfoCache(ProductInfo productInfo){
        String key = PRODUCT_KEY + productInfo.getId();
        ValueOperations<String, ProductInfo> vos = redisTemplate.opsForValue();
        vos.set(key,productInfo);
        log.info("===将商品信息保存至redis缓存-> {}", JSONUtil.toJsonStr(productInfo));
    }
}
