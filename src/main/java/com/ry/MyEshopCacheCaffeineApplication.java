package com.ry;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
@MapperScan("com.ry.common.client.mapper")
public class MyEshopCacheCaffeineApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyEshopCacheCaffeineApplication.class, args);
    }

}
