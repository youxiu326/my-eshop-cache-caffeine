package com.ry.rebuild;

import com.ry.common.client.model.ProductInfo;
import com.ry.common.client.model.ShopInfo;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * 重建缓存的内存队列
 */
public class RebuildCacheQueue {

	private ArrayBlockingQueue<ProductInfo> productQueue = new ArrayBlockingQueue<ProductInfo>(1000);
	private ArrayBlockingQueue<ShopInfo> shopQueue = new ArrayBlockingQueue<ShopInfo>(1000);

	public void putProductInfo(ProductInfo productInfo) {
		try {
			productQueue.put(productInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ProductInfo takeProductInfo() {
		try {
			return productQueue.take();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void putShopInfo(ShopInfo shopInfo) {
		try {
			shopQueue.put(shopInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ShopInfo takeShopInfo() {
		try {
			return shopQueue.take();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 内部单例类
	 * @author Administrator
	 *
	 */
	private static class Singleton {

		private static RebuildCacheQueue instance;

		static {
			instance = new RebuildCacheQueue();
		}

		public static RebuildCacheQueue getInstance() {
			return instance;
		}

	}

	public static RebuildCacheQueue getInstance() {
		return Singleton.getInstance();
	}

}
