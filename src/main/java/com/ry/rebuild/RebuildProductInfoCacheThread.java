package com.ry.rebuild;

import com.ry.common.client.model.ProductInfo;
import com.ry.common.lock.ZookeeperLockUtil;
import com.ry.common.utils.CaffeineCacheUtil;
import com.ry.common.utils.RedisCacheUtil;

import java.time.LocalDateTime;

/**
 * 商品缓存重建线程
 *
 */
public class RebuildProductInfoCacheThread implements Runnable {

    private RedisCacheUtil redisCacheUtil;

    private CaffeineCacheUtil caffeineCacheUtil;

    public RebuildProductInfoCacheThread(RedisCacheUtil redisCacheUtil, CaffeineCacheUtil caffeineCacheUtil) {
        this.redisCacheUtil = redisCacheUtil;
        this.caffeineCacheUtil = caffeineCacheUtil;
    }

    @Override
    public void run() {

        RebuildCacheQueue rebuildCacheQueue = RebuildCacheQueue.getInstance();

        while (true){
            ProductInfo productInfo = rebuildCacheQueue.takeProductInfo();
            Long productId = productInfo.getId();

            try {

                ZookeeperLockUtil.lock(productId);

                ProductInfo existedProductInfo = redisCacheUtil.getProductInfoCache(productId);
                if (existedProductInfo != null){
                    // 比较缓存中存在的更新时间是否已经比我还要更迟
                    // 如果比我的还迟，说明我此时这个重建的请求就可以丢弃了
                    LocalDateTime modifiedTime = productInfo.getModifiedTime();
                    LocalDateTime existedDate = existedProductInfo.getModifiedTime();

                    if(modifiedTime.isAfter(existedDate)) { // 如果当前更新的时间 比 缓存中的更新时间还要之前
                        System.out.println("current date[" + productInfo.getModifiedTime() + "] is before existed date[" + existedProductInfo.getModifiedTime() + "]");
                        return;
                    }
                }else {
                    System.out.println("existed product info is null......");
                }

                caffeineCacheUtil.putProductInfoCache(productInfo);
                redisCacheUtil.putProductInfoCache(productInfo);

            } finally {
                ZookeeperLockUtil.unLock(productId);
            }

        }

    }
}
