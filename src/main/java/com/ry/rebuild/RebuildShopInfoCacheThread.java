package com.ry.rebuild;

import com.ry.common.client.model.ShopInfo;
import com.ry.common.lock.ZookeeperLockUtil;
import com.ry.common.utils.CaffeineCacheUtil;
import com.ry.common.utils.RedisCacheUtil;
import java.time.LocalDateTime;

/**
 * 店铺缓存重建线程
 *
 */
public class RebuildShopInfoCacheThread implements Runnable {

    private RedisCacheUtil redisCacheUtil;

    private CaffeineCacheUtil caffeineCacheUtil;

    public RebuildShopInfoCacheThread(RedisCacheUtil redisCacheUtil, CaffeineCacheUtil caffeineCacheUtil) {
        this.redisCacheUtil = redisCacheUtil;
        this.caffeineCacheUtil = caffeineCacheUtil;
    }

    @Override
    public void run() {

        RebuildCacheQueue rebuildCacheQueue = RebuildCacheQueue.getInstance();

        while (true){
            ShopInfo shopInfo = rebuildCacheQueue.takeShopInfo();
            Long shopId = shopInfo.getId();

            try {

                ZookeeperLockUtil.lock(shopId);

                ShopInfo existedShopInfo = redisCacheUtil.getShopInfoCache(shopId);
                if (existedShopInfo != null){
                    // 比较缓存中存在的更新时间是否已经比我还要更迟
                    // 如果比我的还迟，说明我此时这个重建的请求就可以丢弃了
                    LocalDateTime modifiedTime = shopInfo.getModifiedTime();
                    LocalDateTime existedDate = existedShopInfo.getModifiedTime();

                    if(modifiedTime.isAfter(existedDate)) { // 如果当前更新的时间 比 缓存中的更新时间还要之前
                        System.out.println("current date[" + shopInfo.getModifiedTime() + "] is before existed date[" + existedShopInfo.getModifiedTime() + "]");
                        return;
                    }
                }else {
                    System.out.println("existed product info is null......");
                }

                caffeineCacheUtil.putShopInfoCache(shopInfo);
                System.out.println(caffeineCacheUtil.getShopInfoCache(shopId));

                redisCacheUtil.putShopInfoCache(shopInfo);

            } finally {
                ZookeeperLockUtil.unLock(shopId);
            }

        }

    }
}
