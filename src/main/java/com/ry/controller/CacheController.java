package com.ry.controller;

import cn.hutool.json.JSONUtil;
import com.ry.common.client.BaseProductClient;
import com.ry.common.client.model.ProductInfo;
import com.ry.common.client.model.ShopInfo;
import com.ry.common.utils.CaffeineCacheUtil;
import com.ry.common.utils.RedisCacheUtil;
import com.ry.rebuild.RebuildCacheQueue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
//@RequestMapping("/cache")
public class CacheController {

    @Autowired
    private RedisCacheUtil redisCacheUtil;

    @Autowired
    private CaffeineCacheUtil caffeineCacheUtil;

    @Autowired
    private BaseProductClient baseProductClient;

    /**
     * 查询商品基础信息
     * @param productId 商品id
     */
    @RequestMapping("/getProductInfo")
    @ResponseBody
    public ProductInfo getProductInfo(Long productId) {

        // 先去redis中查询
        ProductInfo productInfo = redisCacheUtil.getProductInfoCache(productId);
        if (productInfo == null){
            // 如果redis中没查询到，避免请求直接打到数据库，去本地缓存查询
            productInfo = caffeineCacheUtil.getProductInfoCache(productId);
            log.info("===从caffeine缓存中获取商品信息-> {}", JSONUtil.toJsonStr(productInfo));
        }

        // 如果缓存中都没有查询到，只能取商品基础服务 查询数据库并返回了
        if (productInfo == null){
            productInfo = baseProductClient.queryProductInfoById(productId);

            // 因为此时缓存中都没有数据了，发送重构缓存请求到内存队列
            RebuildCacheQueue rebuildCacheQueue = RebuildCacheQueue.getInstance();
            rebuildCacheQueue.putProductInfo(productInfo);
        }

        return productInfo;
    }

    /**
     * 查询店铺基础信息
     * @param shopId 店铺id
     */
    @RequestMapping("/getShopInfo")
    @ResponseBody
    public ShopInfo getShopInfo(Long shopId){
        // 先去redis中查询
        ShopInfo shopInfo = redisCacheUtil.getShopInfoCache(shopId);
        if (shopInfo == null){
            // 如果redis中没查询到，避免请求直接打到数据库，去本地缓存查询
            shopInfo = caffeineCacheUtil.getShopInfoCache(shopId);
            log.info("===从caffeine缓存中获取店铺信息-> {}", JSONUtil.toJsonStr(shopInfo));
        }

        // 如果缓存中都没有查询到，只能取商品基础服务 查询数据库并返回了
        if (shopInfo == null){
            shopInfo = baseProductClient.queryShopInfoById(shopId);

            // 因为此时缓存中都没有数据了，发送重构缓存请求到内存队列
            RebuildCacheQueue rebuildCacheQueue = RebuildCacheQueue.getInstance();
            rebuildCacheQueue.putShopInfo(shopInfo);
        }

        return shopInfo;
    }
}
