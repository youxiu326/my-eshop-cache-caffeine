
#### 三级缓存架构


###### 1、当有请求过来，请求商品信息
```java

    1、先去redis缓存查询，发现没有数据
    2、再去caffeine本地缓存查询，发现也没有数据
    3、此时只能直接去商品基础信息服务查询数据，并返回，同时发送一个重建缓存的消息到内存队列中，
    
    有一个监听队列的线程，这时候就会将队列取出，并重建redis缓存和caffeine本地缓存，下次请求再过来，就能从缓存中直接返回了

```


###### 2、当商品基础信息服务 修改了商品信息
```java

    1、商品基础信息服务修改了商品信息，此时就会将修改的商品ID异步发送消息 到Kafka
    2、缓存数据生产服务[当前项目]  会监听kafka topic,当小费到这个消息之后，就会
    取出对应的商品id，进行重建redis缓存和caffeine本地缓存，此时为了避免缓存重建冲突，
    需要进行加锁，且有一个小优化点，比较最近修改的时间，如果已经是落后的时间则忽略。 
```


##### 3、备忘
```java
1、商品详情页缓存数据生产服务的工作流程分析

（1）监听多个kafka topic，每个kafka topic对应一个服务（简化一下，监听一个kafka topic）
（2）如果一个服务发生了数据变更，那么就发送一个消息到kafka topic中
（3）缓存数据生产服务监听到了消息以后，就发送请求到对应的服务中调用接口以及拉取数据，此时是从mysql中查询的
（4）缓存数据生产服务拉取到了数据之后，会将数据在本地缓存中写入一份，就是ehcache中
（5）同时会将数据在redis中写入一份


去监听各种缓存数据对应的原始服务是否有数据变更的消息




第一次访问的时候，其实在nginx本地缓存中是取不到的，所以会发送http请求到后端的缓存服务里去获取，会从redis中获取

拿到数据以后，会放到nginx本地缓存里面去，过期时间是10分钟

然后将所有数据渲染到模板中，返回模板

以后再来访问的时候，就会直接从nginx本地缓存区获取数据了

缓存数据生产 -> 有数据变更 -> 主动更新两级缓存（ehcache+redis）-> 缓存维度化拆分

分发层nginx + 应用层nginx -> 自定义流量分发策略提高缓存命中率

nginx shared dict缓存 -> 缓存服务 -> redis -> ehcache -> 渲染html模板 -> 返回页面

还差最后一个很关键的要点，就是如果你的数据在nginx -> redis -> ehcache三级缓存都不在了，可能就是被LRU清理掉了



```
